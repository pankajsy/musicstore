from django.conf.urls import url

from . import views
app_name = "store"
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^upload_track/$', views.GetTrack.as_view(), name='get_track'),
]