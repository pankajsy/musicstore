from django.contrib import messages
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView
from django.views.generic import FormView
from braces.views import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.conf import settings
from .models import Image
from .forms import Trackform
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth import login, authenticate, logout
from django.views.generic import View, TemplateView
from django.views.generic.edit import UpdateView

class IndexView(View):
    def get(self, request, *args, **kwargs):
        #Checks if the user is authenticated and redirects accordingly
        # if request.user.is_authenticated():
        #     return HttpResponseRedirect(reverse('appsec:list'))
        # else:
        return HttpResponseRedirect(reverse('store:hl_login'))

    # This view is used to upload the images. Django CBV along with Django braces are used for checking authenticity and simplicity
class GetTrack(FormView):
    form_class = Trackform
    template_name = 'track.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            formdata = form.cleaned_data  # Here we collect all the form data and store it to the Image Object
            image = Image()
            image.caption = formdata['caption']
            obj = self.request.FILES['uploadimage']
            image.uploadimage = obj
            owner = User.objects.get(id=request.user.id)
            image.user = owner
            image.save()
            return HttpResponseRedirect(
                reverse('appsec:list'))  # After saving the image object we riderect back to the list view
        return render(request, self.template_name, {'form': form})


# class SearchChurch(APIView):
#     permission_classes = (permissions.IsAuthenticated,)
#
#     def post(self, request, format = None):
#         try:
#             from haystack.query import SearchQuerySet
#             search_string = str(request.POST['search_string'])
#             offset = int(request.POST['offset'])
#             next = str(offset+settings.API_LIMIT_SEARCH_CHURCH)
#             churches_haystack = SearchQuerySet().filter(content = search_string)[offset:offset+settings.API_LIMIT_SEARCH_CHURCH]
#             churches=[]
#             churches_address=[]
#             for i in churches_haystack:
#                 if offset==0:
#                     c = Church.objects.get(name='Default Church')
#                     churches_address.append(c.id)
#                 churches_address.append(i.id)
#             print "GOt churches",churches
#             cs = ChurchSerializer(Church.objects.filter(address__id__in=churches_address),many=True,context={'user_id': int(self.request.POST['user_id'])})
#         except Exception as e:
#             print "Exception"
#             print e
#         return Response({'data': cs.data,'next':next})


# #This view is used to list the images. Django CBV along with Django braces are used for checking authenticity and simplicity
# class ListImage(LoginRequiredMixin, TemplateView):
#     template_name = "imageslist.html"
#     title = "List of Images"
#
#     def get_context_data(self, **kwargs):
#         context = super(ListImage, self).get_context_data(**kwargs)
#         imgs = Image.objects.all().order_by('-pub_date') #We filter the images to view the most recent images first
#         if imgs.count() > 0:
#             paginator = Paginator(imgs, settings.PAGINATION_LIMIT) #Access the PAGE limit set in settings file i.e 10/page
#             page = self.request.GET.get('imagepage')
#             try:
#                 final_images = paginator.page(page) #
#             except PageNotAnInteger:
#                 final_images = paginator.page(1)
#             except EmptyPage:#return rest of the remaining images
#                 final_images = paginator.page(paginator.num_pages)
#             context['images'] = final_images
#         return context