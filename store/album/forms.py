from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from .models import Track
# Form required for Login
class Trackform(forms.ModelForm):
    class Meta:
        model = Track
        fields = ('id','name', 'album' ,'media', 'genre', 'composer', 'millisecond', 'bytes', 'unitprice',)