from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models
import os

#Gets the path for MEDIA_ROOT to the base MEDIA folder, so that the media gets uploaded to the right directory
def get_image_path(instance, filename):
    return os.path.join('store_media', str(instance.id), filename)

#APPUser inherited from the django USER for customized options and adding extra fields.


class MediaType(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        return str(self.name)

class Artist(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        return str(self.name)

class Genre(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        return str(self.name)

class Playlist(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        return str(self.name)

class Employee(models.Model):
    firstname = models.CharField(max_length=200, blank=True, null=True)
    lastname = models.CharField(max_length=200, blank=True, null=True)
    title = models.CharField(max_length=200, blank=True, null=True)
    reportsto = models.CharField(max_length=200, blank=True, null=True)
    birthdate = models.CharField(max_length=200, blank=True, null=True)
    hiredate = models.CharField(max_length=200, blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    country = models.CharField(max_length=200, blank=True, null=True)
    postalcode = models.CharField(max_length=200, blank=True, null=True)
    phone = models.CharField(max_length=200, blank=True, null=True)
    fax = models.CharField(max_length=200, blank=True, null=True)
    email = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        return str(self.firstname)

class Customer(models.Model):
    user = models.OneToOneField(User, related_name='User')
    firstname = models.CharField(max_length=200, blank=True, null=True)
    lastname =  models.CharField(max_length=200, blank=True, null=True)
    company = models.CharField(max_length=200, blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    city = models.CharField(max_length=200, blank=True, null=True)
    state = models.CharField(max_length=200, blank=True, null=True)
    country = models.CharField(max_length=200, blank=True, null=True)
    postalcode = models.CharField(max_length=200, blank=True, null=True)
    phone = models.CharField(max_length=200, blank=True, null=True)
    fax = models.CharField(max_length=200, blank=True, null=True)
    email = models.CharField(max_length=200, blank=True, null=True)
    supportRep = models.ForeignKey(Employee)
    def __str__(self):
        return str(self.firstname)


class Album(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    artist = models.ForeignKey(Artist)
    def __str__(self):
        return str(self.name)

class Track(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    album = models.ForeignKey(Album)
    media = models.ForeignKey(MediaType)
    genre = models.ForeignKey(Genre)
    composer = models.CharField(max_length=200, blank=True, null=True)
    millisecond = models.CharField(max_length=200, blank=True, null=True)
    bytes = models.CharField(max_length=200, blank=True, null=True)
    unitprice = models.CharField(max_length=200, blank=True, null=True)
    def __str__(self):
        return str(self.name)

class Invoice(models.Model):
    invoicedate = models.CharField(max_length=200, blank=True, null=True)
    billingaddress = models.CharField(max_length=200, blank=True, null=True)
    billingcity = models.CharField(max_length=200, blank=True, null=True)
    billingstate = models.CharField(max_length=200, blank=True, null=True)
    billingcountry = models.CharField(max_length=200, blank=True, null=True)
    billingpostalcode = models.CharField(max_length=200, blank=True, null=True)
    total = models.CharField(max_length=200, blank=True, null=True)
    customer = models.ForeignKey(Customer)
    def __str__(self):
        return str(self.firstname)

class InvoiceLine(models.Model):
    unitprice = models.CharField(max_length=200, blank=True, null=True)
    quantity = models.CharField(max_length=200, blank=True, null=True)
    track = models.ForeignKey(Track)
    invoice = models.ForeignKey(Invoice)
    def __str__(self):
        return str(self.firstname)

class PlaylistTrack(models.Model):
    playlist = models.ForeignKey(Playlist)
    track = models.ForeignKey(Track)
    def __str__(self):
        return str(self.name)



#Image model to upload Image to DB with fields specified as follows
#It has a foreing key user. So we can store user as a uploader in every image object
#Basically one user can upload multiple images
#All the images are uploaded to the Appsec_images folder under MEDIA folder
#Caption is editable and the IMages are filtered with their published dates
# class Image(models.Model):
#     customer = models.ForeignKey(Customer)
#     uploadimage = models.ImageField(upload_to='store_images/%Y/%m/%d')
#     caption = models.CharField(max_length=200, blank=True, null=True)
#     pub_date = models.DateTimeField(auto_now_add=True)

